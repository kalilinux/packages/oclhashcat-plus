oclHashcat-plus v0.15
=====================

NV users require ForceWare 319.37 or later
AMD users require Catalyst 13.4 or later

##
## Features
##

- Worlds fastest md5crypt, phpass, mscash2 and WPA / WPA2 cracker
- Worlds first and only GPGPU based rule engine
- Free
- Multi-GPU (up to 128 gpus)
- Multi-Hash (up to 15 million hashes)
- Multi-OS (Linux & Windows native binaries)
- Multi-Platform (OpenCL & CUDA support)
- Multi-Algo (see below)
- Low resource utilization, you can still watch movies or play games while cracking
- Focuses highly iterated modern hashes
- Focuses single dictionary based attacks
- Supports mask attack
- Supports mask files
- Supports distributed cracking
- Supports pause / resume while cracking
- Supports sessions
- Supports restore
- Supports reading words from file
- Supports reading words from stdin
- Supports hex-salt
- Supports hex-charset
- Supports variable rounds where applicable
- Integrated thermal watchdog

##
## Attack-Modes
##

- Straight *
- Combination
- Brute-force
- Hybrid dict + mask
- Hybrid mask + dict

* = Supports rules

##
## Types
##

- MD5
- md5($pass.$salt)
- md5($salt.$pass)
- md5(unicode($pass).$salt)
- md5($salt.unicode($pass))
- SHA1
- sha1($pass.$salt)
- sha1($salt.$pass)
- sha1(unicode($pass).$salt)
- sha1($salt.unicode($pass))
- MySQL
- phpass, MD5(Wordpress), MD5(phpBB3)
- md5crypt, MD5(Unix), FreeBSD MD5, Cisco-IOS MD5
- MD4
- NTLM
- Domain Cached Credentials, mscash
- SHA256
- sha256($pass.$salt)
- sha256($salt.$pass)
- sha256(unicode($pass).$salt)
- sha256($salt.unicode($pass))
- descrypt, DES(Unix), Traditional DES
- md5apr1, MD5(APR), Apache MD5
- SHA512
- sha512($pass.$salt)
- sha512($salt.$pass)
- sha512(unicode($pass).$salt)
- sha512($salt.unicode($pass))
- sha512crypt, SHA512(Unix)
- Domain Cached Credentials2, mscash2
- Cisco-PIX MD5
- WPA/WPA2
- Double MD5
- LM
- Oracle 7-10g, DES(Oracle)
- bcrypt, Blowfish(OpenBSD)
- SHA-3(Keccak)
- Half MD5 (left, mid, right)
- Password Safe SHA-256
- IKE-PSK MD5
- IKE-PSK SHA1
- NetNTLMv1-VANILLA / NetNTLMv1+ESS
- NetNTLMv2
- Cisco-IOS SHA256
- Samsung Android Password/PIN
- RipeMD160
- Whirlpool
- TrueCrypt 5.0+ PBKDF2 HMAC-RipeMD160 + AES
- TrueCrypt 5.0+ PBKDF2 HMAC-SHA512 + AES
- TrueCrypt 5.0+ PBKDF2 HMAC-Whirlpool + AES
- TrueCrypt 5.0+ PBKDF2 HMAC-RipeMD160 boot-mode + AES
- AIX {smd5}
- AIX {ssha256}
- AIX {ssha512}
- 1Password
- AIX {ssha1}
- Lastpass
- GOST R 34.11-94
- OSX v10.8
- GRUB 2
- sha256crypt, SHA256(Unix)
- Joomla
- osCommerce, xt:Commerce
- nsldap, SHA-1(Base64), Netscape LDAP SHA
- nsldaps, SSHA-1(Base64), Netscape LDAP SSHA
- Oracle 11g
- SMF > v1.1
- OSX v10.4, v10.5, v10.6
- MSSQL(2000)
- MSSQL(2005)
- EPiServer 6.x < v4
- EPiServer 6.x > v4
- SSHA-512(Base64), LDAP {SSHA512}
- OSX v10.7
- vBulletin < v3.8.5
- vBulletin > v3.8.5
- IPB2+, MyBB1.2+

##
## Tested OS's
##

- All Windows and Linux versions should work on both 32 and 64 bit

##
## Tested GPU's
##

- All CUDA and Stream enabled cards should work

To get started run the example scripts or check out docs/examples.txt

--
atom
